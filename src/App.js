import React from 'react';
import './App.css';
import fire from './firebase';


export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      confirmResult: null,
      code: "",
    };

  }

  validatePhoneNumber = () => {
    var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/;
    return regexp.test(this.state.phone);
  };

  captchaInit = () => {
    window.recaptchaVerifier = new fire.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
      'callback': function (response) {
        console.log('resolved')
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        this.onSignInSubmit();

      }
    });
  }

  onSignInSubmit = (event) => {
    event.preventDefault();
    this.captchaInit();
    // Request to send OTP
    if (this.validatePhoneNumber()) {

      var phoneNumber = this.state.phone;
      console.log("phone", phoneNumber);
      var appVerifier = window.recaptchaVerifier;
      fire
        .auth()
        .signInWithPhoneNumber(this.state.phone, appVerifier)
        .then((confirmResult) => {
          console.log(confirmResult + "confirmResultdddd");
          this.setState({ confirmResult });
          console.log(confirmResult)
        })
        .catch((error) => {
          alert(error.message);
        });
    } else {
      alert("Invalid Phone Number");
    }
  };


  handleVerifyCode = (event) => {
    event.preventDefault();
    // Request for OTP verification
    const { confirmResult, code } = this.state;
    if (code.length === 6) {
      confirmResult.confirm(code).then(function (result) {
        // User signed in successfully.
        var user = result.user;
        console.log(user)
        alert("OTP Verified")
        // ...
      }).catch(function (error) {
        alert(error.message)
        // User couldn't sign in (bad verification code?)
        // ...
      });
    } else {
      alert("Please enter a 6 digit OTP code.");
    }
  };



  handleChange(event) {
    this.setState({ phone: event.target.value });
  }
  handleChangetwo(event) {
    this.setState({ code: event.target.value });
    console.log(event)
  }

  render() {
    return (
      <div>

        <div id="recaptcha-container"></div>
      
        <div className="login-otp">
          <h3 className="mb-2"> {this.state.confirmResult != null ? 'Enter OTP' : 'Login With Phone Number'} </h3>
          {this.state.confirmResult == null && <form onSubmit={this.onSignInSubmit}>
            <input type="text"
              value={this.state.phone} className="form-control" placeholder="+91 XXXXXXXXXX" onChange={this.handleChange.bind(this)}
            />
            <button type="submit" className="btn btn-primary mt-2 mb-2"> Generate OTP </button>
          </form>
          }

          {this.state.confirmResult != null && <div className="otp-verify">
            <form onSubmit={this.handleVerifyCode}>
              <input className="form-control" placeholder="Enter OTP" type="text"
                value={this.state.code}
                onChange={this.handleChangetwo.bind(this)}
              />
              <button type="submit" className="btn btn-primary mt-2 mb-2"> verify OTP </button>
            </form>
          </div>
          }
        </div>
      </div>

    );
  }
}